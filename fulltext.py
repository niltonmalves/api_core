from glob import glob
import json, re
from utils.util import create_list_of_lengths,verify_number_of_lines, run_transform_text_in_line
import pandas as pd

""" this files is to transform the fullText in one line document 
And rewrite all fullText in a txt file. Each line in the txt file
is a fullText of a article.
Then we verify if all documents contains the same number of lines
"""
# def create_list_of_lengths(file):
#     return [len(line)  for line in open(file, encoding="utf8")]


files = glob('./json_files/*_3200.json')

"""get the fullText from each article in the json files
and create one line document for each article"""
run_transform_text_in_line(files)

clean_list =glob('./txt_files/clean_fulltext_*.txt')
l_total = []
for file in clean_list:
    l1 = create_list_of_lengths(file)  #calcule the length of of fullText and generete a list of length
    l_total = l_total + l1
    print(sorted(l1)[0:5])

# this `sorted(l_total)` we can see fulltext length    
print(sorted(l_total))

print(pd.Series(sorted(l_total)).value_counts())

#verify_number_of_lines
#should print the same number for all
verify_number_of_lines(clean_list)

