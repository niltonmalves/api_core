# API_CORE

Download json files using API dataSet_Core

## Getting started

run ```python main.py```

----------------------------------------------------------------
## fullText

To transform the fullText in one line document and rewrite all fullText in a txt file. Each line in the txt file is a fullText of a article.
Then we verify if all documents contains the same number of lines

run ```python fullText.py```

---------------
## Other options

To Other actions

run ```python read_jsons_count_ids.py ```
