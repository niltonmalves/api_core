import json
from glob import glob
import pandas as pd
from pathlib import Path
import re
from sqlalchemy import null

# this part is to avoid do the same job twice
# because we will run this script many times.

filename = Path('files_worked.txt')
filename.touch(exist_ok=True)
files = glob('./json_files/*_3200.json')
# print(files)
all = files
files_writed = [line.split("\n")[0] for line in open("files_worked.txt")]
done = files_writed
to_do = list(set(all)-set(done))
# print('to_do is: ',to_do)


ids = []
# fulltext = []
abstract = []
language = []
file_name_source = []
for file in to_do:
    f = open(file, encoding="utf8")
    data= json.load(f)

    for key in data['id']:
        ids.append(data['id'][key])
        # print(data['fullText'][key][0:5])
        if not data['fullText'][key]:
            print('ok')
        # fulltext.append(data['fullText'][key])
        abstract.append(data['abstract'][key])
        if data['language'][key] != None:
            language.append(data['language'][key]['name'])
        else:
            language.append(None)
        file_name_source.append(file)

        # print(data['id'][key])
    # for file in files:
    #     pass
# print(data['id'])
df_ids = pd.Series(ids, name='id')
df_language = pd.Series(language, name='language')
# df_fulltext = pd.Series(fulltext, name='fulltext')
df_abstract = pd.Series(abstract, name='abstract')
df_file_name_source = pd.Series(file_name_source, name='file_name')

# print("df_fulltext.isna().istrue(): ",df_fulltext.isna().describe())

print("df_abstract.isna().istrue():\n",df_abstract.isna().describe())

temp = df_abstract.isna().describe()
number_of_abstracts = temp.freq
total = temp._values[0]
number_of_null_abstracts = total - number_of_abstracts
print("number_of_abstracts is : ",number_of_abstracts)
print('number_of_null_abstracts is: ', number_of_null_abstracts)


lista_language = open('lista_language.txt', 'a')
for i in language:
    # print(i)
    lista_language.write(str(i)+"\n")
lista_language.close()


ln = [line.split('\n')[0] for line in  open('lista_language.txt')]
print(ln[0:2])

print(pd.Series(ln).describe(include=all), '\n******\n')

print(pd.Series(ln).value_counts())


textfile = open("files_worked.txt", "a")
for element in to_do:
    textfile.write(element + "\n")
textfile.close()



# textfile = open("files_worked.txt", "w")
# for element in files:
#     textfile.write(element + "\n")
# textfile.close()

# all = files



# # print(list(set(all)-set(done)))

# # f = open(files[0], encoding="utf8")
# # data= json.load(f)


print('len(ids): ', len(ids))

print('set : ', len(list(set((ids)))))