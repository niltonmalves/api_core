import json
from glob import glob
import pandas as pd

files = glob('./json_files/*.json')
print(files)


all = files
files_writed = [line.split("\n")[0] for line in open("files_worked.txt")]

done = files_writed
to_do = list(set(all)-set(done))
print(to_do)


ids = []
fulltext = []
abstract = []
language = []
file_name_source = []
for file in to_do:
    f = open(file, encoding="utf8")
    data= json.load(f)

    for key in data['id']:
        ids.append(data['id'][key])
        fulltext.append(data['fullText'][key])
        abstract.append(data['abstract'][key])
        if data['language'][key] != None:
            language.append(data['language'][key]['name'])
        else:
            language.append(None)
        file_name_source.append(file)

        # print(data['id'][key])
    # for file in files:
    #     pass
# print(data['id'])
df_ids = pd.Series(ids)
df_language = pd.Series(language)
df_fulltext = pd.Series(fulltext)
df_abstract = pd.Series(abstract)
df_file_name_source = pd.Series(file_name_source)




textfile = open("files_worked.txt", "a")
for element in to_do:
    textfile.write(element + "\n")
textfile.close()



# textfile = open("files_worked.txt", "w")
# for element in files:
#     textfile.write(element + "\n")
# textfile.close()

# all = files



# # print(list(set(all)-set(done)))

# # f = open(files[0], encoding="utf8")
# # data= json.load(f)


# print('len(ids): ', len(ids))

# print('set : ', len(list(set((ids)))))