#!/usr/bin/env python
# coding: utf-8

from matplotlib import rcParams
import requests
import json
import seaborn as sns
import itertools
from datetime import datetime
import matplotlib.pyplot as plt
import networkx as nx
import hashlib
import os
from operator import itemgetter
import pandas
import time
from random import randrange, uniform

api_key = "lU8aSNb6LfdjtnCiMQWxAqE5mvYXhrIH"
api_endpoint = "https://api.core.ac.uk/v3/"

def pretty_json(json_object):
    print(json.dumps(json_object, indent=2))

def query_api(url_fragment, query,is_scroll=False, limit=200, scrollId=None):
    headers={"Authorization":"Bearer "+api_key}
    query = {"q":query, "limit":limit}
    if not is_scroll:
        response = requests.post(f"{api_endpoint}{url_fragment}",data = json.dumps(query), headers=headers)
    elif not scrollId:
        query["scroll"]="true"
        response = requests.post(f"{api_endpoint}{url_fragment}",data = json.dumps(query),headers=headers)
    else:
        query["scrollId"]=scrollId
        response = requests.post(f"{api_endpoint}{url_fragment}",data = json.dumps(query),headers=headers)
    if response.status_code ==200:
        return response.json(), response.elapsed.total_seconds()
    else:
        print(f"Error code {response.status_code}, {response.content}")

def scroll(search_url, query, extract_info_callback=None,last_scrollId=None):
    count = 0
    scrollId=None
    try:
            
        with open ("count.txt", "r") as cnt:
            if cnt.readlines()[0].strip() == None:
                count = 0
                print('type of count in if: ', type(count))
            else:
                print('type of count in else :', type(count))
                count=cnt.readlines()[0].strip()
    except:
        pass

    with open ("last_scrollId.txt", "r") as lastScrollId:
        lastSId=lastScrollId.readlines()[0].strip()
        if lastSId == None:
            print('if')
            # do something
        else:
            print('else')
            # use lastSId
    
    while True:
        with open('count.txt', 'w') as c:
            c.write(str(count))
        for i in range(0,9):
            print('i is==> ', i)
            allresults = []
            result, elapsed =query_api(search_url, query, is_scroll=True, scrollId=scrollId)
            scrollId=result["scrollId"]
            print(scrollId)
            totalhits = result["totalHits"]
            result_size = len(result["results"])
            if result_size==0:
                break
            for hit in result["results"]:
                if extract_info_callback:
                    allresults.append(extract_info_callback(hit))
                else:
                    allresults.append(hit)
            count+=result_size
            print(f"{count}/{totalhits} {elapsed}s")
            df=pandas.DataFrame(allresults)
            #write scrollId to use if break
            with open('last_scrollId.txt', 'w') as si:
                si.write(scrollId)
            with open(f"{count}_{totalhits}_3200.json", "w",encoding='utf-8') as file:
                df.to_json(file,force_ascii=False, indent = 2 )
        #sleep one minute to overcome
        #API request limit
        time.sleep(70)

def get_data_providers_id(hit):
    return hit

scroll("search/works", "'oil and gas' OR petroleum", get_data_providers_id)

# with open ("last_scrollId.txt", "r") as lastScrollId:
#     lastSId=lastScrollId.readlines()[0].strip()

# print(lastSId == None)