import json, re
import os

def get_filename_without_extension(file_path):
    file_name = os.path.basename(file_path)
    index_of_dot = file_name.index('.')
    file_name_without_extension = file_name[:index_of_dot]
    return file_name_without_extension

def create_list_of_lengths(file):
    """calcule the length of of fullText and generete a list of length"""
    return [len(line)  for line in open(file, encoding="utf8")]

def run_transform_text_in_line(list_of_json_files):
    """get the fullText from each article in the json files
    and create one line document for each article"""
    count = 0
    for file in list_of_json_files:
        file_name_without_extension = get_filename_without_extension(file)    
        lista_fulltext = open(f'./txt_files/clean_fulltext_{file_name_without_extension}_{count}.txt', 'a', encoding="utf8")
        count +=1
        f = open(file, encoding="utf8")
        data= json.load(f)
        for key in data['id']:
            string_full = data['fullText'][key]
            sem_barra_n = re.sub('\n', ' ', string_full)
            retira_espaco_extra = re.sub('\s+',' ', sem_barra_n)
            lista_fulltext.write(retira_espaco_extra+"\n")
        lista_fulltext.close()
    
def verify_number_of_lines(list_of_files):
    """para conferir se o numero de linhas é compativel com
o numero de documentos"""
    for file in list_of_files:
        print(sum(1  for line in open(file, encoding="utf8")), ' lines in this file: ', file)
